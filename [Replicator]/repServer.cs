﻿// Replicator.dll
// © Vladimir Shulzhitskiy, 2015
// repServer.cs

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using App = Autodesk.AutoCAD.ApplicationServices;
using Db = Autodesk.AutoCAD.DatabaseServices;
using Ed = Autodesk.AutoCAD.EditorInput;
using Rtm = Autodesk.AutoCAD.Runtime;

namespace _Replicator_
{
    public class repServer
    {
        private Db.ObjectIdCollection ObjIDCol;
        private string Appfullname;

        [Rtm.CommandMethod("repserverstart")]
        public void repserverstart()
        {
            App.Document acDoc = App.Application.DocumentManager.MdiActiveDocument;
            Db.Database acCurDb = acDoc.Database;
            Ed.Editor acEd = acDoc.Editor;
            Appfullname = CalculateMD5Hash(acDoc.Name);
            
            string dirname = acDoc.Name + ".rep";
            if (System.IO.Directory.Exists(dirname))
            {
                string[] files_del = Directory.GetFiles(dirname, "*");
                foreach (string sourceFileName in files_del)
                {
                    System.IO.File.Delete(sourceFileName);
                }
            }
            ObjIDCol = new Db.ObjectIdCollection();
            acCurDb.ObjectAppended += new Db.ObjectEventHandler(acMessage_ObjectAppended);
            acCurDb.ObjectModified += new Db.ObjectEventHandler(acMessage_ObjectModified);
            acCurDb.ObjectErased += new Db.ObjectErasedEventHandler(acMessage_ObjectErased);
            acDoc.CommandEnded += new App.CommandEventHandler(acMessage_CommandEnded);
            acDoc.CommandCancelled += new App.CommandEventHandler(acMessage_CommandEnded);
            acEd.WriteMessage("\nRepServer Started");
        }
        [Rtm.CommandMethod("repserverstop")]
        public void repserverstop()
        {
            App.Document acDoc = App.Application.DocumentManager.MdiActiveDocument;
            Db.Database acCurDb = acDoc.Database;
            Ed.Editor acEd = acDoc.Editor;
            string dirname = acDoc.Name + ".rep";
            Appfullname = String.Empty;

            if (System.IO.Directory.Exists(dirname))
            {
                string[] files_del = Directory.GetFiles(dirname, "*");
                foreach (string sourceFileName in files_del)
                {
                    System.IO.File.Delete(sourceFileName);
                }
            }
            try
            {
                System.IO.Directory.Delete(dirname);
            }
            catch (System.IO.IOException)
            {
                acEd.WriteMessage("\nУдаление временных файлов не завершено. Папка используется еще одним процессом.");
            }
            ObjIDCol.Clear();
            acCurDb.ObjectAppended -= new Db.ObjectEventHandler(acMessage_ObjectAppended);
            acCurDb.ObjectModified -= new Db.ObjectEventHandler(acMessage_ObjectModified);
            acCurDb.ObjectErased -= new Db.ObjectErasedEventHandler(acMessage_ObjectErased);
            acDoc.CommandEnded -= new App.CommandEventHandler(acMessage_CommandEnded);
            acDoc.CommandCancelled -= new App.CommandEventHandler(acMessage_CommandEnded);
            acEd.WriteMessage("\nRepServer Stopped");
        }
        void acMessage_ObjectAppended(object senderObj, Db.ObjectEventArgs evtArgs){acMessageObj(evtArgs.DBObject.ObjectId, "ObjectAppended: ");}
        void acMessage_ObjectModified(object senderObj, Db.ObjectEventArgs evtArgs){acMessageObj(evtArgs.DBObject.ObjectId, "ObjectModified: ");}
        void acMessage_ObjectErased(object senderObj, Db.ObjectErasedEventArgs evtArgs){acMessageDel(evtArgs.DBObject.ObjectId, "ObjectErased: ");}
        void acMessage_CommandEnded(object sender, App.CommandEventArgs e)
        {
            App.Document acDoc = App.Application.DocumentManager.MdiActiveDocument;
            Db.Database sourceDb = acDoc.Database;
            Ed.Editor acEd = acDoc.Editor;
            string filefullname = acDoc.Name;

            FileInfo tempFilenfo = new FileInfo(filefullname);
            string dirname = filefullname + ".rep";
            if (!System.IO.Directory.Exists(dirname))
            {
                System.IO.Directory.CreateDirectory(dirname);
            }
            using (App.DocumentLock acLckDoc = acDoc.LockDocument())
            {
                Db.ObjectIdCollection sourceIds = new Db.ObjectIdCollection();
                foreach (Db.ObjectId i in ObjIDCol)
                {
                    Db.ObjectId id = Db.ObjectId.Null;
                    if (sourceDb.TryGetObjectId(i.Handle, out id))
                    {
                        sourceIds.Add(id);
                    }
                }
                if (sourceIds.Count > 0)
                {
                    try
                    {
                        using (Db.Database destDb = new Db.Database(true, true))
                        {
                            Db.ObjectId sourceMsId = Db.SymbolUtilityServices.GetBlockModelSpaceId(sourceDb);
                            Db.ObjectId destDbMsId = Db.SymbolUtilityServices.GetBlockModelSpaceId(destDb);
                            Db.ObjectIdCollection sourceIds_1 = new Db.ObjectIdCollection();
                            using (Db.Transaction acTrans = sourceDb.TransactionManager.StartTransaction())
                            {
                                foreach (Db.ObjectId i in sourceIds)
                                {
                                    Db.DBObject get_object = acTrans.GetObject(i, Db.OpenMode.ForRead, true);
                                    if (get_object.OwnerId.IsEffectivelyErased == false)
                                    {
                                        get_object.UpgradeOpen();
                                        Db.RegAppTable regTable = (Db.RegAppTable)acTrans.GetObject(sourceDb.RegAppTableId, Db.OpenMode.ForRead);
                                        if (!regTable.Has(Appfullname))
                                        {
                                            regTable.UpgradeOpen();
                                            Db.RegAppTableRecord app = new Db.RegAppTableRecord();
                                            app.Name = Appfullname;
                                            regTable.Add(app);
                                            acTrans.AddNewlyCreatedDBObject(app, true);
                                        }
                                        get_object.XData = new Db.ResultBuffer(new Db.TypedValue(1001, Appfullname),
                                                                new Db.TypedValue(1000, get_object.Handle.Value.ToString()));
                                        get_object.DowngradeOpen();
                                        sourceIds_1.Add(get_object.ObjectId);
                                    }
                                }
                                acTrans.Commit();
                            }
                            Db.IdMapping mapping = new Db.IdMapping();
                            try
                            {
                                sourceDb.WblockCloneObjects(sourceIds_1, destDbMsId, mapping, Db.DuplicateRecordCloning.Replace, false);
                                destDb.SaveAs(dirname +"\\" +Path.GetRandomFileName() + ".dwg", Db.DwgVersion.Current);
                            }
                            catch (Autodesk.AutoCAD.Runtime.Exception ex)
                            {
                                acEd.WriteMessage("\n Ошибка сериализации объекта: ");
                                acEd.WriteMessage("\n "+ ex.Message);
                            }
                        }
                    }
                    catch (System.Exception eXP)
                    {
                        System.Windows.Forms.MessageBox.Show(eXP.ToString());
                    }
                    sourceIds.Clear();
                    ObjIDCol.Clear();
                }
            }
        }
        void acMessageObj(Db.ObjectId ID, String msg)
        {
            App.Document acDoc = App.Application.DocumentManager.MdiActiveDocument;
            Db.Database acCurDb = acDoc.Database;
            Ed.Editor acEd = acDoc.Editor;
            if (ID.ObjectClass.IsDerivedFrom(Rtm.RXClass.GetClass(typeof(Db.Entity))))
            {
                using (Db.Transaction acTrans = acCurDb.TransactionManager.StartOpenCloseTransaction())
                {
                    Db.DBObject get_object = null;
                    try
                    {
                        get_object = ID.Open(Db.OpenMode.ForRead, true, true);
                    }
                    catch
                    {
                        get_object = ID.Open(Db.OpenMode.ForNotify, true, true);
                    }
                        if (!ObjIDCol.Contains(ID))
                        {
                            ObjIDCol.Add(ID);
                        }
                }
            }
        }
        void acMessageDel(Db.ObjectId ID, String msg)
        {
            App.Document acDoc = App.Application.DocumentManager.MdiActiveDocument;
            Db.Database acCurDb = acDoc.Database;
            Ed.Editor acEd = acDoc.Editor;
                if (ID.ObjectClass.IsDerivedFrom(Rtm.RXClass.GetClass(typeof(Db.Entity))))
            {
                string fgnsfg = ID.ObjectClass.DxfName.ToString();
                using (Db.Transaction acTrans = acCurDb.TransactionManager.StartOpenCloseTransaction())
                {
                     Db.DBObject get_object= null;
                    try
                    {
                        get_object = ID.Open(Db.OpenMode.ForRead, true, true);
                    }
                    catch
                    {
                        get_object = ID.Open(Db.OpenMode.ForNotify,true,true);
                    }

                    if (get_object.IsErased == true )
                    {
                        string filefullname = acDoc.Name;
                        //string Appfullname = acDoc.Name.Replace('\\', '_');
                        //string Appfullname = CalculateMD5Hash(acDoc.Name);
                        FileInfo tempFilenfo = new FileInfo(filefullname);
                        string dirname = filefullname + ".rep";
                        if (!System.IO.Directory.Exists(dirname))
                        {
                            System.IO.Directory.CreateDirectory(dirname);
                        }
                        string filename = dirname + "\\" + get_object.Handle.Value.ToString() + ".del";
                        if (!File.Exists(filename))
                        {
                            using (FileStream fs = File.Create(filename))
                            {
                            }
                        }
                    }
                    if (get_object.IsUndoing == true)
                    {
                        ObjIDCol.Add(ID);
                    }
                }
            }
        }

        public static string CalculateMD5Hash(string input)
        {
            string hashString = "rep";
            using (MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hash = md5.ComputeHash(inputBytes);
                hashString = Convert.ToBase64String(hash).Replace('/', '_').Replace('+', '-');
                hashString = hashString.Substring(0, hashString.Length - 2);
                if (hashString.Length > 30)
                    hashString = hashString.Substring(0, 30);
            }
            return hashString;
        }
    }
}
