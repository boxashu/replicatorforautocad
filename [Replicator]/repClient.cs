﻿// Replicator.dll
// © Vladimir Shulzhitskiy, 2015
// repClient.cs

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using App = Autodesk.AutoCAD.ApplicationServices;
using Db = Autodesk.AutoCAD.DatabaseServices;
using Ed = Autodesk.AutoCAD.EditorInput;
using Rtm = Autodesk.AutoCAD.Runtime;

namespace _Replicator_
{
    public class repClient
    {
        private static  string Appfullname;

        static Dictionary<string, Db.Handle> listReplicator = null;
        static Control syncCtrl = null;
        static FileSystemWatcher watcher = null;
        static repClient()
        {
            syncCtrl = new Control();
            syncCtrl.CreateControl();
            Appfullname = _Replicator_.repServer.CalculateMD5Hash("000");
        }
        ~repClient()
        {
            if (watcher != null)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Dispose();
                watcher = null;
            }
        }
        private static void OnChangedEventProcW(object obj, FileSystemEventArgs e)
        {
            watcher.EnableRaisingEvents = false;
            BackgroundProcessW();
            watcher.EnableRaisingEvents = true;
        }
        delegate void FinishedProcessingDelegate();
        private static void FindMyObject(Db.Database acCurDb, string Appfullname)
        {
            using (Db.Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
                {
                    Db.BlockTable acBlkTbl;
                    acBlkTbl = acTrans.GetObject(acCurDb.BlockTableId, Db.OpenMode.ForRead) as Db.BlockTable;
                    Db.BlockTableRecord acBlkTblRec;
                    acBlkTblRec = acTrans.GetObject(acBlkTbl[Db.BlockTableRecord.ModelSpace], Db.OpenMode.ForRead) as Db.BlockTableRecord;
                    foreach (Db.ObjectId asObjId in acBlkTblRec)
                    {
                        try
                        {
                            Db.Entity ent = (Db.Entity)acTrans.GetObject(asObjId, Db.OpenMode.ForRead);
                            Db.ResultBuffer XdataInfo = ent.GetXDataForApplication(Appfullname);
                            if (XdataInfo != null)
                            {
                                foreach (Db.TypedValue tv in XdataInfo)
                                {
                                    if (tv.TypeCode == 1000)
                                    {
                                        if (listReplicator.ContainsKey(tv.Value.ToString()) == false)
                                        {
                                            listReplicator.Add(tv.Value.ToString(), ent.Handle);
                                        }
                                    }
                                }
                            }
                        }
                        catch (System.Exception eXP)
                        {
                            System.Windows.Forms.MessageBox.Show(eXP.ToString());
                        }
                    }
                    acTrans.Commit();
                }
        }
        static void FinishedProcessing()
        {
            App.Document acDoc = App.Application.DocumentManager.MdiActiveDocument;
            Db.Database acCurDb = acDoc.Database;
            Ed.Editor acEd = acDoc.Editor;
            string filefullname = acDoc.Name;
            FileInfo tempFilenfo = new FileInfo(filefullname);
            string dirname = filefullname + ".rep";
            if (System.IO.Directory.Exists(dirname))
            {
            using (App.DocumentLock locDoc = acDoc.LockDocument())
            {
                FindMyObject(acCurDb, Appfullname);
                DirectoryInfo di = new DirectoryInfo(dirname);
                FileSystemInfo[] filesInfo = di.GetFileSystemInfos();
                var orderedFiles = filesInfo.OrderBy(f => f.CreationTime).Select(f => f.FullName);
                string[] files = orderedFiles.ToArray();
                foreach (string sourceFileName in files)
                {
                    if (sourceFileName.Substring(sourceFileName.Length-4,4).ToUpper() == ".del".ToUpper())
                    {
                        try
                        {
                            using (Db.Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
                            {
                                FileInfo finf = new FileInfo(sourceFileName);
                                string hnd = Path.GetFileNameWithoutExtension(sourceFileName);
                                Db.ObjectId idd = Db.ObjectId.Null;
                                if (listReplicator.ContainsKey(hnd))
                                {
                                    if (acCurDb.TryGetObjectId(listReplicator[hnd], out idd))
                                    {
                                        Db.Entity ent_loc = (Db.Entity)acTrans.GetObject(idd, Db.OpenMode.ForWrite,true,true);
                                        if (!ent_loc.IsErased)
                                        {
                                            ent_loc.Erase();
                                            acTrans.Commit();
                                            listReplicator.Remove(hnd);
                                        }    
                                    }
                                }
                            }
                        }
                        catch (System.Exception eXP)
                        {
                            System.Windows.Forms.MessageBox.Show(eXP.ToString());
                        }
                        System.IO.File.Delete(sourceFileName);
                    }
                    if (sourceFileName.Substring(sourceFileName.Length - 4, 4).ToUpper() == ".dwg".ToUpper())
                    {
                        try
                        {
                            using (Db.Database sourceDb = new Db.Database(true, true))
                            {
                                bool notFree = true;
                                while (notFree)
                                {
                                    try
                                    {
                                        sourceDb.ReadDwgFile(sourceFileName, FileShare.Read, true, string.Empty);
                                        notFree = false;
                                    }
                                    catch (Autodesk.AutoCAD.Runtime.Exception)
                                    {
                                        System.Threading.Thread.Sleep(200);
                                        notFree = true;
                                    }
                                }
                                Db.ObjectId sourceMsId = Db.SymbolUtilityServices.GetBlockModelSpaceId(sourceDb);
                                Db.ObjectId destDbMsId = Db.SymbolUtilityServices.GetBlockModelSpaceId(acCurDb);
                                Db.ObjectIdCollection sourceIds = new Db.ObjectIdCollection();
                                using (Db.BlockTableRecord ms = sourceMsId.Open(Db.OpenMode.ForRead) as Db.BlockTableRecord)
                                {
                                    using (Db.Transaction acTrans = sourceDb.TransactionManager.StartTransaction())
                                    {
                                        foreach (Db.ObjectId id in ms)
                                        {
                                            try
                                            {
                                                Db.Entity ent = (Db.Entity)acTrans.GetObject(id, Db.OpenMode.ForRead);
                                                Db.ResultBuffer XdataInfo = ent.GetXDataForApplication(Appfullname); 
                                                if (XdataInfo != null)
                                                {
                                                    foreach (Db.TypedValue tv in XdataInfo)
                                                    {
                                                        if (tv.TypeCode == 1000)
                                                        {
                                                            if (listReplicator.ContainsKey(tv.Value.ToString()) == true)
                                                            {
                                                                using (Db.Transaction acTrans_loc = acCurDb.TransactionManager.StartTransaction())
                                                                {
                                                                    Db.ObjectId  idd = Db.ObjectId.Null;
                                                                    if (acCurDb.TryGetObjectId(listReplicator[tv.Value.ToString()], out idd))
                                                                    {
                                                                        Db.Entity ent_loc = null;
                                                                         ent_loc = (Db.Entity)acTrans_loc.GetObject(idd, Db.OpenMode.ForWrite,true,true);
                                                                        if (!ent_loc.IsErased)
                                                                        {
                                                                            ent_loc.Erase();
                                                                            acTrans_loc.Commit();
                                                                            listReplicator.Remove(tv.Value.ToString());
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            catch (System.Exception eXP)
                                            {
                                                System.Windows.Forms.MessageBox.Show(eXP.ToString());
                                            }
                                            sourceIds.Add(id);
                                        }
                                        acTrans.Commit();
                                    }
                                    Db.IdMapping mapping = new Db.IdMapping();
                                    sourceDb.WblockCloneObjects(sourceIds, destDbMsId, mapping, Db.DuplicateRecordCloning.Replace, false);
                                }
                            }
                        }
                        catch (System.Exception eXP)
                        {
                            System.Windows.Forms.MessageBox.Show(eXP.ToString());
                        }
                }
                    System.IO.File.Delete(sourceFileName);
                    App.Application.UpdateScreen();
                    acEd.UpdateScreen();
                    acEd.Regen();
                }
            }
        }
        }
        static void BackgroundProcessW()
        {
            if (syncCtrl.InvokeRequired)
                syncCtrl.Invoke(
                  new FinishedProcessingDelegate(FinishedProcessing));
            else
                FinishedProcessing();
        }
        [Rtm.CommandMethod("repClientStart")]
        public void repClientStartW()
        {
            listReplicator = new Dictionary<string, Db.Handle>();
            if (watcher != null)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Dispose();
                watcher = null;
            }
            App.Document acDoc = App.Application.DocumentManager.MdiActiveDocument;
            Db.Database acCurDb = acDoc.Database;
            Ed.Editor acEd = acDoc.Editor;
            string filefullname = acDoc.Name;
            Appfullname = _Replicator_.repServer.CalculateMD5Hash(acDoc.Name);
            FileInfo tempFilenfo = new FileInfo(filefullname);
            string dirname = filefullname + ".rep";

            if (System.IO.Directory.Exists(dirname))
            { 
                watcher = new FileSystemWatcher();
                watcher.Path=dirname;
                watcher.NotifyFilter = NotifyFilters.FileName;
                watcher.Changed += new FileSystemEventHandler(OnChangedEventProcW);
                watcher.Created += new FileSystemEventHandler(OnChangedEventProcW);
                watcher.EnableRaisingEvents = true;
            }
            acEd.WriteMessage("\nRepClient Started");
        }
        [Rtm.CommandMethod("repClientStop")]
        public void repClientStop()
        {
            Appfullname = System.String.Empty;
            listReplicator.Clear();
            if (watcher != null)
            {
                watcher.EnableRaisingEvents = false;
                watcher.Dispose();
                watcher = null;
            }
            App.Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("\nRepClient Stopped");
        }
    }
}
