# README #

This README would normally document whatever steps are necessary to get your application up and running.

Так как все началось с этих тем:http://adn-cis.org/forum/index.php?topic=971.msg4262#msg4262,  http://adn-cis.org/forum/index.php?topic=971.msg4422#msg4422) 

**Описание библиотеки:**

Программа для совместной работы над чертежом.

1) на компьютере \\A пользователь А открывает автокадом сетевой .dwg файл (скажем) w:/rrr.dwg

2) Акад настроен так, что плагин загружается автоматически , командой repServerStart, пользователь активирует для нужного чертежа режим *Сервера*.

3) на компьютере \\B пользователь Б открывает автокадом сетевой .dwg тот же файл w:/rrr.dwg

4) Акад настроен так, что плагин загружается автоматически, командой repClientStart, пользователь активирует для нужного чертежа режим *Клиента*

5) При создании/удалении/модификации/ восстановлении после удаления пользователем A
- любых простых (не составных) графических примитивов (производные от Autodesk.AutoCAD.DatabaseServi ces.Curve)

6) Срабатывают реакторы плагина на изменение

7) Изменения отображаются в автокаде у пользователя Б.

**Ограничения.**

1. В виду того что работа ведется только с простыми объектами, то при изменении цвета или слоя существующего объекта возможно вызовет зависание автокада

2. Сервер должен стартовать первым, если этого не так, то необходимо выполнить реинициализацию клиента.
Реинициализация выполняется остановкой клиента (команда repClientStop) и повторным запуском клиента (команда repClientStart)
3. Сервер должен иметь право записи в сетевую папку в которой расположен рабочий файл.[/spoiler]

**Реализованные команды:**

* repServerStart – запуск сервера

* repServerStop – остановка сервера

* repClientStart – запуск клиента

* repClientStop – остановка клиента

Видео пример работы этой библиотеки тут: https://www.youtube.com/watch?v=uNz3Co1lfDU

  
Извиняюсь за отсутствие комментов =(

Конструктивная критика очень приветствуется, я ведь только учусь.


**И я неприменно дооформлю этот readme**


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact